from flask import Flask
import subprocess

# pimoroni imports
# particulate sensor
from pms5003 import PMS5003, ReadTimeoutError
from enviroplus import gas
import logging
########

application = Flask(__name__)

######
logging.basicConfig(
    format='%(asctime)s.%(msecs)03d %(levelname)-8s %(message)s',
    level=logging.INFO,
    datefmt='%Y-%m-%d %H:%M:%S')

logging.info("app started")

pms5003 = PMS5003()
#######

@application.route("/v1/gas")
def gas_route():
    # QUESTION: are there any exceptions to handle?
    readings = gas.read_all()
    logging.info(readings)

    detailsToReturn = {"adc":readings.adc,
                       "nh3":readings.nh3,
                       "oxidising":readings.oxidising,
                       "reducing":readings.reducing}

    # QUESTION: do we want to use jsonify?
    return detailsToReturn

@application.route("/v1/particulates")
def particulates_route():
    global pms5003

    try:
        readings = pms5003.read()
        logging.info(readings)
    except ReadTimeoutError:
        # TODO probably don't need to do this as it's from the example loop
        # should definitely print out the error though
        # QUESTION: should we handle the other possible errors
        pms5003 = PMS5003()

    detailsToReturn = {"PM1_0":readings.pm_ug_per_m3(1.0),
                       "PM2_5":readings.pm_ug_per_m3(2.5),
                       "PM10":readings.pm_ug_per_m3(10),
                       "PM1_0_atm":readings.pm_ug_per_m3(1.0, True),
                       "PM2_5_atm":readings.pm_ug_per_m3(2.5, True),
                       "PM10_atm":readings.pm_ug_per_m3(None, True),
                       "gt0_3um":readings.pm_per_1l_air(0.3),
                       "gt0_5um":readings.pm_per_1l_air(0.5),
                       "gt1_0um":readings.pm_per_1l_air(1.0),
                       "gt2_5um":readings.pm_per_1l_air(2.5),
                       "gt5_0um":readings.pm_per_1l_air(5.0),
                       "gt10um":readings.pm_per_1l_air(10.0)}

    # QUESTION: do we want to use jsonify?
    return detailsToReturn

@application.route("/v1/serial")
def serial_route():
    result = subprocess.run("cat /sys/firmware/devicetree/base/serial-number",
            check=True, shell=True, stdout=subprocess.PIPE,
            stderr=subprocess.PIPE)

    returnString = result.stdout.decode("utf-8").rstrip("\x00")
    logging.info(returnString)

    return returnString

if __name__ == "__main__":
  application.run(debug=True, host='0.0.0.0')

