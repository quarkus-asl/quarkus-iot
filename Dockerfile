from registry.gitlab.com/quarkus-asl/quarkus-iot/qiot-sensor-service-base:1-aarch64

RUN dnf -y install git && dnf clean all

RUN git clone https://gitlab.com/quarkus-asl/quarkus-iot.git

RUN pip3 install -r quarkus-iot/requirements.txt

ENTRYPOINT ["python"]

CMD ["quarkus-iot/application.py"]

